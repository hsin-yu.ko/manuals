# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018-2023, Inkscape Documentation Authors
# This file is distributed under the same license as the Inkscape Beginners'
# Guide package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Inkscape Beginners' Guide 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-11 16:48+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: zh_TW\n"
"Language-Team: zh_TW <LL@li.org>\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.1\n"

#: ../../source/copying-a-color.rst:3
msgid "Copying a Color"
msgstr ""

#: ../../source/copying-a-color.rst:5
msgid "There are several ways to apply the same style to multiple objects:"
msgstr ""

#: ../../source/copying-a-color.rst:7
msgid ""
"**Selecting multiple objects**: select all the objects whose color you "
"want to change (hold :kbd:`Shift` and click, or drag a selection box "
"around them), then open the :guilabel:`Fill and Stroke` dialog, select a "
"color. It will be applied to all selected objects automatically."
msgstr ""

#: ../../source/copying-a-color.rst:10
msgid ""
"**Use the Dropper tool**: select an object, then activate the Dropper "
"tool by clicking on its icon |Dropper tool icon|, then click on the "
"canvas, on an area where it has the color that you want to apply. The "
"selected object's color will change accordingly."
msgstr ""

#: ../../source/copying-a-color.rst:21
msgid "Dropper tool icon"
msgstr ""

#: ../../source/copying-a-color.rst:12
msgid ""
"**Copy the complete style of an object to apply it to another object:** "
"select the object, copy it to the clipboard with :kbd:`Ctrl` + :kbd:`C`, "
"then select the second object and paste only the style with :kbd:`Shift` "
"+ :kbd:`Ctrl` + :kbd:`V`. This not only copies the fill and stroke color,"
" but also the stroke width and style, a text's font style and any "
"patterns or gradients."
msgstr ""

