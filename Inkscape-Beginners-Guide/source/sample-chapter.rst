*************************
Sample Chapter Title (h1)
*************************

.. Inkscape version 1.3 (please add the corresponding Inkscape version number to the file, so everyone can see quickly whether it is up-to-date or may need an update)

.. TODO: Please preface comments with items that still need work in a file with the word 'TODO', so they can be found quickly.

This documentation uses 'reStructured Text' (reST) as its markup language. The below document outlines most of the elements used in this guide.

If you have never used reST before, please familiarize yourself with its syntax
(links to cheatsheets below), or diligently copy-paste what you see here or in
other parts of the documentation. Note that spaces (and how many there are) are
always relevant. Without them, the automatical document creator does not
understand what you mean, and may give unexpected results. Backticks (this one:
\`, not apostrophes: ') are used frequently in delimiting different elements.

Second level heading (h2)
=========================

It's best to have this document opened as the finished web page, and as the
source text in parallel. That way you can see the syntax, and how it will look
when the documentation has been auto-created.

This is text in a paragraph. Separate paragraphs by an empty line. **This** is
printed bold, and *this* is in italics. ``This`` is for code and other small
commandline commands that are used inline.

Third level heading (h3)
------------------------

Links to cheatsheets in a list:

  - `Structure Overview <https://raw.githubusercontent.com/rtfd/sphinx_rtd_theme/master/docs/demo/structure.rst>`__
  - `Paragraph Level Markup Overview <https://raw.githubusercontent.com/rtfd/sphinx_rtd_theme/master/docs/demo/demo.rst>`__
  - `Lists & Tables Overview <https://raw.githubusercontent.com/rtfd/sphinx_rtd_theme/master/docs/demo/lists_tables.rst>`__

Fourth level heading (h4)
^^^^^^^^^^^^^^^^^^^^^^^^^

A numbered list with useful links:

#. `Inkscape Website <https://inkscape.org>`__

   This is a paragraph in a list

#. An internal link to another book chapter uses its file name (without the '.rst'): :doc:`Link to 3D Boxes <3d-boxes>`

#. If you want to link to another section in this manual (this can be in any file, or in this one), you must put a so called label there (with underscore).

.. _example:

  #. Then you write the :ref:`link to that label <example>` (no underscore) here.

#. If you want to refer to a section that starts with a heading, you do not need to use a label. You can just directly refer to it like this: `Third level heading (h3)`_.

#. A download link, e.g. for an example file that users can play with, will look like this: :download:`Link to Glyph A file <../image_sources/glyph_a.svg>`

--------------

Images can be used within a sentence or line, or as a larger image with a description.

An image within a sentence can be something like this icon |Icon for Selector tool|. You decide on a name for the image that you put into the text. Then, you specify its properties in a separate block. It's best to put these blocks at the bottom of the document (scroll down to the end to see it). This type of image can be described as an 'inline image'.

--------------

All images that show a part of the Inkscape interface should be formatted like this and have the class 'screenshot', a caption and an alt attribute. Screenshots should be in the png file format and the relevant area should be as large as possible:

.. figure:: images/calligraphy_tool_tracing.png
    :alt: This text will be shown if the image cannot be loaded
    :class: screenshot

    This is a caption for the image. Text should wrap around the caption. You can add a 'target' line above, if you want to link to something.

.. container:: two-column

   .. figure:: images/pattern02.png
       :alt: This text will be shown if the image cannot be loaded
       :class: screenshot

       You can also use a `two-column` container, and the figures …

   .. figure:: images/pattern04.png
       :alt: This text will be shown if the image cannot be loaded
       :class: screenshot

       … will fit on one or two columns, depending on the display
       width.

--------------

Images that are used for mere decoration look like this. You can use png images. If you want to use an SVG image, you must also provide a pdf with the same content and give it the same name. Then, you would need to replace the file extension by an asterisk in the page source.

.. image:: images/vector-format.png
   :class: deco left small

Available classes:

- for alignment: left, center, right
- for size: small, medium, large

--------------

If you want to tell the user how to get somewhere using the menu, use the menu selection markup, e.g. :menuselection:`Edit --> Preferences --> Input/Output --> Autosave`

--------------

For a label in the interface, use the gui label markup, e.g. :guilabel:`Enable gradient editing`.

--------------

If you want to tell the user which key to press, use the keyboard shortcut markup, e.g. :kbd:`Ctrl` + :kbd:`C`.

--------------

For referencing a specific file on the user's computer, use the markup for files, e.g. :file:`~/.config/inkscape/preferences.xml`

--------------

If you need to quote someone, or another document, do this by indenting by 4 spaces:

    Inkscape is professional quality vector graphics software which runs on
    Windows, Mac OS X and GNU/Linux. It is used by design professionals and
    hobbyists worldwide, for creating a wide variety of graphics such as
    illustrations, icons, logos, diagrams, maps and web graphics. Inkscape uses
    the W3C open standard SVG (Scalable Vector Graphics) as its native format,
    and is free and open-source software.

    -- Inkscape Website

--------------

We can have different styles of admonition boxes:

.. Attention:: Watch out for this!

.. Caution:: Be careful!

.. WARNING:: Drawing can be addictive.

.. DANGER:: Don't break Inkscape!

.. Error:: Now you have it.

.. Hint:: The status line in Inkscape is very useful.

.. Important::
   - Start your computer.
   - Login.
   - Start Inkscape.
   - Select the rectangle tool.

.. Tip:: Give the user a tip to have an easier time

    +----------+
    | with     |
    +==========+
    | things   |
    +----------+
    | in       |
    +----------+
    | Inkscape |
    +----------+

.. Note:: This is a note.
   You can add more contents below.
   Really.


.. admonition:: And, by the way...

   You can make up your own admonitions titles, too. However, you cannot choose their color.

--------------

Referring to **terms** in the :doc:`glossary <glossary>` is a good way to help users have quick access to the help they need, e.g. you can use :term:`Path`: (if you want to keep the exact spelling) or :term:`path <Path>` (if you want to change the spelling, e.g. for plural, or to not use capitalization).

--------------

In case you ever want to put in a longer **code block**:

.. code-block:: bash
   :linenos:

   sudo apt-get install inkscape
   flatpak install inkscape
   snap install inkscape

--------------

Dashes and other typography:

Please follow English typography guidelines for dashes.
See https://en.wikipedia.org/wiki/Dash

En dash: –

Em dash: —

Ellipsis: …

--------------

The following won't be visible in this location, but it will appear in any place where you write its 'anchor'. Use the asterisk instead of the file ending, if there is both an svg and a pdf of the image file available. This should be the case for all icons of the Inkscape interface, as there is a complete set of them in the :file:`icons` directory:

.. |Icon for Selector tool| image:: images/icons/tool-pointer.*
   :class: inline

.. |Test Icon| image:: images/icons/tool-pointer.*
   :class: header-icon
