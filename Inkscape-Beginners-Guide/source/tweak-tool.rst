**************
The Tweak Tool
**************

|Icon for Tweak Tool| :kbd:`Shift` + :kbd:`F2` or :kbd:`W`

The Tweak tool is the ideal tool for illustrators who first draw their designs on
paper, then scan and vectorize their work. With it, you can modify paths
on the canvas, using numerous options, without ever
switching tools.

As the tool only works on **selected objects**, first select the path that you would like to modify. Then switch to the **Tweak tool** and **click** on the area that you wish to edit, or **click and drag** the tool over the canvas.

First, let's have a look at its general options:

Width
  Determines the size of the tool. If you only want to edit
  a small region on the canvas, make the circle smaller, else, make it
  bigger.

Force
  Determines how strong the effect of the tool will be.

|Icon for activating pressure sensitivity| Pressure sensivity
  Activate this button to use the pressure sensitivity of your graphics tablet to
  dynamically change the force of the applied effect. However, the tool can be
  used well without a graphics tablet.

Now let's look at the heart of the tool, its different modes:

|Icon for pushing objects with Tweak Tool| |Icon for attracting objects with Tweak Tool| |Icon for moving objects randomly with Tweak Tool| Move objects
  The first three icons move the selected objects in various directions. Read
  the hint in the status bar to learn about additional key presses that change
  how the tool works.

|Icon for shrinking objects with Tweak Tool| Shrink/Grow objects
  Reduces the size of the paths, or, when you hold down :kbd:`Shift`,
  enlarges the paths.

|Icon for rotating objects with Tweak Tool| Rotate
  Rotates the paths.

|Icon for duplicating objects with Tweak Tool| Duplicate
  Creates duplicates of the selected paths. These will be placed directly above
  the originals, so remember to move them, if you want to see the effect.

|Icon for pushing path parts with Tweak Tool| Push path parts
  Deforms the paths by pushing the nodes like a snow shovel.

|Icon for shrinking parts of paths with Tweak Tool| Shrink /|Icon for growing parts of paths with Tweak Tool| grow path parts
  The borders of a part of a path will be moved closer to each
  other, with :kbd:`Shift`, they will be moved farther away from each other.

|Icon for roughening paths with Tweak Tool| Roughen path parts
  Roughens the contours of a path.

|Icon for painting objects with Tweak Tool| Paint color and |Icon for randomizing colors of objects with Tweak Tool| Jitter colors
  These modes modify the color of the paths.

|Icon for blurring objects with Tweak Tool| Blur objects
  Adds blur to the paths.

|Chameleon image to be tweaked|

This drawing will serve as an example. The chameleon was drawn with a
pencil, and afterwards it has been vectorized with Inkscape.

|Chameleon tail editing with tweak tool|

The tail needs to be edited, so it will gradually become thinner. Here,
the effect has been exaggerated for demonstration purposes.

|The chameleon's tongue is being roughened with Tweak Tool| Some
roughening has been added to the tongue, to emphasize the beast's
dangerous side.

.. |Icon for Tweak Tool| image:: images/icons/tool-tweak.*
   :class: header-icon
.. |Icon for activating pressure sensitivity| image:: images/icons/draw-use-pressure.*
   :class: inline
.. |Icon for pushing objects with Tweak Tool| image:: images/icons/object-tweak-push.*
   :class: inline
.. |Icon for attracting objects with Tweak Tool| image:: images/icons/object-tweak-attract.*
   :class: inline
.. |Icon for moving objects randomly with Tweak Tool| image:: images/icons/object-tweak-randomize.*
   :class: inline
.. |Icon for shrinking objects with Tweak Tool| image:: images/icons/object-tweak-shrink.*
   :class: inline
.. |Icon for rotating objects with Tweak Tool| image:: images/icons/object-tweak-rotate.*
   :class: inline
.. |Icon for duplicating objects with Tweak Tool| image:: images/icons/object-tweak-duplicate.*
   :class: inline
.. |Icon for pushing path parts with Tweak Tool| image:: images/icons/path-tweak-push.*
   :class: inline
.. |Icon for shrinking parts of paths with Tweak Tool| image:: images/icons/path-tweak-shrink.*
   :class: inline
.. |Icon for growing parts of paths with Tweak Tool| image:: images/icons/path-tweak-grow.*
   :class: inline
.. |Icon for roughening paths with Tweak Tool| image:: images/icons/path-tweak-roughen.*
   :class: inline
.. |Icon for painting objects with Tweak Tool| image:: images/icons/object-tweak-paint.*
   :class: inline
.. |Icon for randomizing colors of objects with Tweak Tool| image:: images/icons/object-tweak-jitter-color.*
   :class: inline
.. |Icon for blurring objects with Tweak Tool| image:: images/icons/object-tweak-blur.*
   :class: inline
.. |Chameleon image to be tweaked| image:: images/tweak_tool_chameleon_orig.png
.. |Chameleon tail editing with tweak tool| image:: images/tweak_tool_chameleon_shrink_tail.png
.. |The chameleon's tongue is being roughened with Tweak Tool| image:: images/tweak_tool_chameleon_roughen_tongue.png
