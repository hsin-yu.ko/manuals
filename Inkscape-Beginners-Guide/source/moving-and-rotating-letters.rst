***************************
Moving and Rotating Letters
***************************

Aside from deforming a text with the :guilabel:`Selector` tool, Inkscape offers numerous options to have fun with texts, adapting them to the design,
while keeping them editable.

To use the following options you need to select the text, words, or
letters you want to move with the text tool, and then increase or
decrease the value of the option in the tool controls bar (the default
value for all of them is 0). The change will immediately be visible on
the canvas.

Options for moving and rotating letters
=======================================

Changing spacing between letters
--------------------------------

The option allows you to change the distances between all letters in the
selected text, or the selected letters. This is useful if your font size is very small, because it improves readability.

Changing spacing between words
------------------------------

Here you can change the distances between all words in your text, or the
selected part of the text.

Changing horizontal kerning
---------------------------

The option changes the distance between the two letters in the position of
the cursor. All letters that come after will move by the value you indicate
in the input field.

Changing vertical kerning
-------------------------

One of the options is for changing the position of a word or single letters
in relation to the base line. This means that the selected word or letter
will be moved up or down, and will no longer rest on the same line as its
fellows. 

Changing letter rotation
-------------------------
One last option allows you to also rotate single letters (or all selected
letters).

If your design requires a more radical change of the shape of the text,
it is possible to make a text follow a path or to put it into an
arbitrarily shaped frame.

.. figure:: images/text_menu_letters_spacing.png
    :alt: Options of the text tool that change the letters' positions.
    :class: screenshot

    Options of the text tool that change the letters' positions.

.. container:: two-column

   .. figure:: images/text_standard.png
       :alt: Standard text.
       :class: screenshot

       Standard text.

   .. figure:: images/text_kerning.png
       :alt: Text with extra spacing applied.
       :class: screenshot

       Text with a little kerning added.

.. figure:: images/text_typographical_effects.png
    :alt: Adjusting the height to achieve typographical effects.
    :class: screenshot

    Text with exaggerated kerning and rotation.
