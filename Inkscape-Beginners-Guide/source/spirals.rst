*******
Spirals
*******

|Icon for Spiral Tool| :kbd:`F9` or :kbd:`I`

This geometrical shape isn't needed frequently, but sometimes, it proves
to be very useful.

To draw a spiral, click and drag with the mouse on the canvas. When the left mouse button is released, the spiral will be finished. You will notice two diamond-shaped handles on the spiral.

These handles change the length of the spiral. You can try this out
right on the canvas: Just grab one of the handles and drag it along the
spiral's turns to get the desired number of turns and to make the spiral
larger or smaller.

If you hold down :kbd:`Ctrl` while dragging the handles, the spiral will get
longer (or shorter) in 15° steps.


When you combine pressing :kbd:`Alt` with pulling the inner handle
upwards or downwards, it will change the divergence (tightness) /
convergence (looseness) of the spiral, without changing its overall size.
Dragging upwards makes the turns move toward the outside of the spiral. 
Dragging downwards will make them move closer to its center.

The easiest way to change the number of turns of a spiral quickly by a
large amount is to enter the desired number into the field labelled
:guilabel:`Turns` in the tool controls bar. This will not change the spiral's
diameter.

If you ever feel lost when working with the spiral tool, you can use the
|Reset Icon| rightmost icon in the tool controls bar to remove all changes and to
reset the spiral to its initial shape.

.. container:: two-column

   .. figure:: images/spiral_tool_variations_1.png
      :alt: Spiral tool
      :class: screenshot

      A basic spiral

   .. figure:: images/spiral_tool_variations_2_handles.png
      :alt: Spiral tool: changing with the handles
      :class: screenshot

      Changing a spiral using its handles.

   .. figure:: images/spiral_tool_variations_3_converge.png
      :alt: Spiral tool: drag inner handle down to converge
      :class: screenshot

      :kbd:`Alt` + dragging the inner handle downwards makes the spiral converge more.

   .. figure:: images/spiral_tool_variations_4_diverge.png
      :alt: Spiral tool: drag inner handle up to diverge
      :class: screenshot

      :kbd:`Alt` + dragging the inner handle upwards lets the spiral more divergent.

   .. figure:: images/spiral_tool_variations_5_many.png
      :alt: Spiral tool: large number of turns
      :class: screenshot

      A spiral with a large number of turns.

   .. figure:: images/spiral_tool_variations_6_few.png
      :alt: Spiral tool: small number of turns
      :class: screenshot

      A spiral with a smaller number of turns.


.. |Icon for Spiral Tool| image:: images/icons/draw-spiral.*
   :class: header-icon
.. |Reset Icon| image:: images/icons/edit-clear-symbolic.*
   :class: inline
