*****************
Node Tool Options
*****************

|Icon for Node Tool| :kbd:`F2` or :kbd:`N`

The Node tool offers a number of options we haven't seen yet. In this
chapter we will go through them, by looking at the different icons in
its tool controls bar:

|Icon for inserting nodes| Insert new node
  A double-click on a path segment lets you add new nodes easily directly on the
  canvas. If you need to add more nodes, or want to insert a node right in the
  middle between two other nodes, you can click on the path segment or select
  multiple nodes, then use this button. More nodes allow you to more
  precisely determine the shape of a path.

|Icon for deleting nodes| Delete selected nodes
  Select the node(s) and then either use the :kbd:`Del` key, or use this button.
  You should aim for having as few nodes as possible, so you can
  make changes to the objects in your drawing more quickly.

|Icon for merging nodes| Join (merge) nodes
  Select at least two nodes. When you click on the button, the nodes will be
  merged into a single node. Inkscape will try to preserve the path's shape as
  well as possible.

|Icon for splitting nodes| Break path
  This will split one node into two nodes. These two new nodes are not connected
  by a path segment. The new nodes only have a single handle, as they are end
  nodes, and they are placed directly on top of each other. This can sometimes
  be difficult to handle. Only use this feature when you really need it!

|Icon for joining nodes with a segment| Join end nodes with a new segment
  This is ideal for manually joining separate path pieces. If you select all nodes in a path that has many interruptions, this will always join the two nodes that are closest to each other.

|Icon for deleting a segment between two nodes| Delete segment between two non-end nodes
  Breaks a path in two, leaving a gap.

The next couple of icons can be used to **convert** one thing into a
different one:

|Cusp node icon| |Smooth node icon| |Symmetric node icon| |Auto-smooth node icon| Convert different node types into each other
  see :doc:`chapter 'About Node Types' <node-types>`

|Icon for converting objects to paths| Convert objects to paths
  We learn about this in the :doc:`chapter about how to edit the nodes on a geometrical shape <objects-to-paths>`

|Icon for converting strokes to paths| Convert stroke to path
  Convert strokes of objects into separate objects

Then, there are number fields for changing the x and y coordinate of the
selected node, and a drop-down menu that allows you to change the unit
for the coordinates.

These buttons determine if certain path properties will be editable and
visible on the canvas:

|Show clip icon| |Show mask icon| |Show LPE parameter icon| |Show transform handles icon| |Show Bezier handles icon| |Show outline icon|

.. 
   TODO: exchange a couple of the images. No node types, but segments and some of the other operations mentioned above.

.. figure:: images/node_tool_node_added_example.png
   :alt: A new node has been added
   :class: screenshot

   An object where a node has been added by double-clicking on the path.

.. figure:: images/node_tool_split_node_example.png
   :alt: Stacked nodes
   :class: screenshot

   A stroke was added, and another node. This node was broken into two.
   This one looks a lot smaller. Make sure you really want this before
   clicking the button!

.. figure:: images/node_tool_delete_segment_example.png
   :alt: Segment was deleted
   :class: screenshot

   The segment between two nodes has been deleted.

.. figure:: images/node_tool_node_editing_1_example.png
   :alt: Before editing
   :class: screenshot

   Example of editing a path by …

.. figure:: images/node_tool_node_editing_2_example.png
   :alt: Moving a node
   :class: screenshot

   … moving a node, then …

.. figure:: images/node_tool_node_editing_3_example.png
   :alt: Changing node type
   :class: screenshot

   … changing a node's type and modifying the handles.



.. |Icon for Node Tool| image:: images/icons/tool-node-editor.*
   :class: header-icon
.. |Icon for inserting nodes| image:: images/icons/node-add.*
   :class: inline
.. |Icon for deleting nodes| image:: images/icons/node-delete.*
   :class: inline
.. |Icon for merging nodes| image:: images/icons/node-join.*
   :class: inline
.. |Icon for splitting nodes| image:: images/icons/node-break.*
   :class: inline
.. |Icon for joining nodes with a segment| image:: images/icons/node-join-segment.*
   :class: inline
.. |Icon for deleting a segment between two nodes| image:: images/icons/node-delete-segment.*
   :class: inline
.. |Cusp node icon| image:: images/icons/node-type-cusp.*
   :class: inline
.. |Smooth node icon| image:: images/icons/node-type-smooth.*
   :class: inline
.. |Symmetric node icon| image:: images/icons/node-type-symmetric.*
   :class: inline
.. |Auto-smooth node icon| image:: images/icons/node-type-auto-smooth.*
   :class: inline
.. |Icon for converting objects to paths| image:: images/icons/object-to-path.*
   :class: inline
.. |Icon for converting strokes to paths| image:: images/icons/stroke-to-path.*
   :class: inline
.. |Show clip icon|  image:: images/icons/path-clip-edit.*
   :class: inline
.. |Show mask icon|  image:: images/icons/path-mask-edit.*
   :class: inline
.. |Show LPE parameter icon|  image:: images/icons/path-effect-parameter-next.*
   :class: inline
.. |Show transform handles icon|  image:: images/icons/node-transform.*
   :class: inline
.. |Show Bezier handles icon|  image:: images/icons/show-node-handles.*
   :class: inline
.. |Show outline icon| image:: images/icons/show-path-outline.*
   :class: inline
